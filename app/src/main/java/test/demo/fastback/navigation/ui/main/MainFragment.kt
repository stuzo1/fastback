package test.demo.fastback.navigation.ui.main

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import test.demo.fastback.navigation.LOG_TAG
import test.demo.fastback.navigation.R

class MainFragment : Fragment() {

    private val navController by lazy { findNavController() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.main_fragment, container, false)
        view.findViewById<Button>(R.id.start).setOnClickListener { startTesting() }
        return view
    }

    private fun startTesting() {
        navController.navigate(MainFragmentDirections.actionMainFragmentToFirstTestFragment())
    }

    //Logging

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d(LOG_TAG, "onAttach ${this::class.simpleName}")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(LOG_TAG, "onCreate ${this::class.simpleName}")
    }

    override fun onPause() {
        super.onPause()
        Log.d(LOG_TAG, "onPause ${this::class.simpleName}")
    }

    override fun onResume() {
        super.onResume()
        Log.d(LOG_TAG, "onResume ${this::class.simpleName}")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(LOG_TAG, "onViewCreated ${this::class.simpleName}")
    }

    override fun onStart() {
        super.onStart()
        Log.d(LOG_TAG, "onStart ${this::class.simpleName}")
    }

    override fun onStop() {
        super.onStop()
        Log.d(LOG_TAG, "onStop ${this::class.simpleName}")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(LOG_TAG, "onDestroyView ${this::class.simpleName}")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(LOG_TAG, "onDestroy ${this::class.simpleName}")
    }

    override fun onDetach() {
        super.onDetach()
        Log.d(LOG_TAG, "onDetach ${this::class.simpleName}")
    }
}