package test.demo.fastback.navigation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.fragment.NavHostFragment
import test.demo.fastback.navigation.ui.main.MainFragment

const val LOG_TAG = "XXX"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.d(LOG_TAG, "act onBackPressed")
    }
}